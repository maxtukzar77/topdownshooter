// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSGameInstance.h"

bool UTPSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;
	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponInfoTable - NULL"));
	return bIsFind;
}

bool UTPSGameInstance::GetProjectileInfoByName(FName NameProjectile, FProjectileInfo& OutInfo)
{
	bool bIsFind = false;
	FProjectileInfo* ProjectileInfoRow;
	if (ProjectileInfoTable)
	{
		ProjectileInfoRow = ProjectileInfoTable->FindRow<FProjectileInfo>(NameProjectile, "", false);
		if (ProjectileInfoRow)
		{
			bIsFind = true;
			OutInfo = *ProjectileInfoRow;
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetProjectileInfoByName - ProjectileInfoTable - NULL"));
	return bIsFind;
}

bool UTPSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;
	FDropItem* DropInfoRow;
	if (DropItemInfoTable)
	{
		TArray<FName> RowNames = DropItemInfoTable->GetRowNames();
		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropInfoRow->WeaponInfo.NameItem == NameItem)
			{
				bIsFind = true;
				OutInfo = *DropInfoRow;
			}
			i++;
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetDropItemInfoByName - DropItemInfoTable - NULL"));
	return bIsFind;
}
