// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Source/Weapons/Magazine.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "SleeveBullets.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ASleeveBullets : public AMagazine
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASleeveBullets();

	UPROPERTY(VisibleAnywhere, BluePrintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UProjectileMovementComponent* Movement = nullptr;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
