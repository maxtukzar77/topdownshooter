// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/Projectiles/ProjectileDefault.h"
#include "GrenadeDefault.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API AGrenadeDefault : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DelataTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void ImpactProjectile() override;

	void Explose();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;

	bool TimerEnabled = false;
	float TimerToExpolse = 0.0f;
};
