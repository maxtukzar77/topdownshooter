// Fill out your copyright notice in the Description page of Project Settings.


#include "GrenadeDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

void AGrenadeDefault::BeginPlay()
{
	Super::BeginPlay();
}


void AGrenadeDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AGrenadeDefault::TimerExplose(float DelataTime)
{
	if(TimerEnabled)
	{
		if(TimerToExpolse >= ProjectileSettings.TimeToExplose)
			Explose();
		else
			TimerToExpolse += DelataTime;
	}
}

void AGrenadeDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AGrenadeDefault::ImpactProjectile()
{
	TimerEnabled = true;
}

void AGrenadeDefault::Explose()
{
	TimerEnabled = false;
	if(ProjectileSettings.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExploseFX, GetActorLocation(), GetActorRotation());		
	}
	if(ProjectileSettings.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSettings.ExploseMaxDamage,
		ProjectileSettings.ExploseMinDamage,
		GetActorLocation(),
		ProjectileSettings.ExploseMinRadiusDamage,
		ProjectileSettings.ExploseMaxRadiusDamage,
		5,
		NULL, IgnoredActor, nullptr, nullptr);

	if (ShowDebug)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExploseMinRadiusDamage, 32, FColor::Red, false, 5.0f, (uint8)'\000', 5.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExploseMaxRadiusDamage, 32, FColor::Blue, false, 5.0f, (uint8)'\000', 5.0f);
	}

	this->Destroy();
}
