// Fill out your copyright notice in the Description page of Project Settings.
#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "SleeveBullets.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Game/TPSGameInstance.h"
#include "Character/TPSInventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	DropMagazineLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropMagazineLocation"));
	DropMagazineLocation->SetupAttachment(RootComponent);

	SleeveBulletsLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("SleeveBulletsLocation"));
	SleeveBulletsLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (FireTimer < 0.f)
	{
		if (WeaponFiring)
		{
			if (GetWeaponRound() > 0)
			{
				if (!WeaponReloading)
					Fire();
			}
			else
			{
				if (!WeaponReloading && CheckCanWeaponReload())
					InitReload();
			}
		}
	}
	else
		FireTimer -= DeltaTime;
		
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if(WeaponReloading)
	{
		if(ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
			CurrentDispersion = CurrentDispersionMin;
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
				CurrentDispersion = CurrentDispersionMax;
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current - %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
	//FireTimer = 0.01f;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FProjectileInfo myProjectileInfo;
	if (myGI->GetProjectileInfoByName(WeaponSetting.ProjectileID, myProjectileInfo))
		return myProjectileInfo;
	return FProjectileInfo();
}


void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	WeaponInfo.Round--;
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentLocation());

	int8 NumberProjectile = GetNumberProjectileByShot();
	
	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			if (ProjectileInfo.Projectile)
			{
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(ProjectileInfo);
				}
			}
			else
			{
				FHitResult Hit;
				if (GetWorld()->LineTraceSingleByChannel(Hit, SpawnLocation, EndLocation, ECollisionChannel::ECC_Visibility))
				{
					AActor* OtherActor = Hit.GetActor();
					UPrimitiveComponent* OtherComp = Hit.GetComponent();
					if (OtherActor != nullptr)
					{
						//if (OtherActor && Hit.PhysMaterial.IsValid())
						{
							EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

							if (WeaponSetting.HitDecals.Contains(mySurfacetype))
							{
								UMaterialInterface* myMateral = WeaponSetting.HitDecals[mySurfacetype];

								if (myMateral && OtherComp)
								{
									UGameplayStatics::SpawnDecalAttached(myMateral, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
								}
							}
							if (WeaponSetting.HitFXs.Contains(mySurfacetype))
							{
								UParticleSystem* myParticle = WeaponSetting.HitFXs[mySurfacetype];
								if (myParticle)
								{
									UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
								}
							}
							if (WeaponSetting.HitSound)
							{
								UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.HitSound, Hit.ImpactNormal);
							}
						}
						UGameplayStatics::ApplyDamage(OtherActor, WeaponSetting.WeaponDamage, GetInstigatorController(), this, NULL);
					}
				}
			}
		}
		if (WeaponSetting.AnimCharFire)
		{
			OnWeaponFire.Broadcast(WeaponSetting.AnimCharFire, WeaponSetting.AnimCharFireAim, WeaponSetting.RateOfFire);
			SleeveBulletsDropTimer = (WeaponSetting.TimeToDropSleeveBullets / WeaponSetting.AnimCharFire->GetPlayLength()) * WeaponSetting.RateOfFire;
		}
		if (WeaponSetting.SleeveBullets)
		{
			if (SleeveBulletsDropTimer != 0)
				GetWorldTimerManager().SetTimer(DropTimer, this, &AWeaponDefault::DropSleeveBullets, SleeveBulletsDropTimer);
			else
				DropSleeveBullets();
		}
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovmentState NewMovmentState)
{
	BlockFire = false;

	switch (NewMovmentState)
	{
	case EMovmentState::WalkAim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.WalkAim_DispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.WalkAim_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.WalkAim_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.WalkAim_DispersionReduction;
		break;
	case EMovmentState::RunAim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.RunAim_DispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.RunAim_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.RunAim_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.RunAim_DispersionReduction;
		break;
	case EMovmentState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_DispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_DispersionReduction;
		break;
	case EMovmentState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_DispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_DispersionReduction;
		break;
	case EMovmentState::Sprint_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.0f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanseTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32,FColor::Emerald, false, 5.0f, (uint8)'\000', 5.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanseTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, 5.0f, (uint8)'\000', 5.0f);
	}

	if (ShowDebug)
	{
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Blue, false, 5.0f, (uint8)'\000', 5.0f);
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.0f, (uint8)'\000', 5.0f);
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.0f, (uint8)'\000', 5.0f);
	}
	return EndLocation;
}

int32 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;


	if (WeaponSetting.AnimCharReload)
	{
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload, WeaponSetting.AnimCharReloadAim, WeaponSetting.ReloadTime);
		MagazineDropTimer = (WeaponSetting.TimeToDropMagazine / WeaponSetting.AnimCharReload->GetPlayLength()) * WeaponSetting.ReloadTime;
	}
	if (WeaponSetting.MagazineReload)
	{
		if (MagazineDropTimer != 0)
			GetWorldTimerManager().SetTimer(DropTimer, this, &AWeaponDefault::DropMagazine, MagazineDropTimer);
		else
			DropMagazine();
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	int8 AviableAmmoForReload = GetAviableAmmoForReload();
	
	int32 AmmoNeedTaken = WeaponSetting.MaxRound - WeaponInfo.Round;
	if (AmmoNeedTaken > AviableAmmoForReload)
		AmmoNeedTaken = AviableAmmoForReload;
	WeaponInfo.Round += AmmoNeedTaken;

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTaken);
    FireTimer = 0.01f;
}

void AWeaponDefault::CanceleReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd.Broadcast(false, 0);
	GetWorldTimerManager().ClearTimer(DropTimer);
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UTPSInventoryComponent* myInv = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (myInv)
		{
			int8 kek;
			if (!myInv->CheckAmmoOnWeapon(WeaponSetting.WeaponType, kek))
				result = false;
		}
	}
	return result;
}

int8 AWeaponDefault::GetAviableAmmoForReload()
{
	int8 result = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UTPSInventoryComponent* myInv = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (myInv)
		{
			myInv->CheckAmmoOnWeapon(WeaponSetting.WeaponType, result);
		}
	}
	return result;
}

void AWeaponDefault::DropMagazine()
{
	FVector SpawnLocation = DropMagazineLocation->GetComponentLocation();
	FRotator SpawnRotation;
	if (StaticMeshWeapon)
		SpawnRotation = StaticMeshWeapon->GetComponentRotation();
	else if(SkeletalMeshWeapon)
		SpawnRotation = SkeletalMeshWeapon->GetComponentRotation();

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();

	AMagazine* myMagazine = Cast<AMagazine>(GetWorld()->SpawnActor(AMagazine::StaticClass(), &SpawnLocation, &SpawnRotation, SpawnParams));
	if (myMagazine)
	{
		myMagazine->SetStaticMesh(WeaponSetting.MagazineReload);
		myMagazine->SetLifeSpan(5);
	}
}

void AWeaponDefault::DropSleeveBullets()
{
	FVector SpawnLocation = SleeveBulletsLocation->GetComponentLocation();
	FRotator SpawnRotation = SleeveBulletsLocation->GetComponentRotation();

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();

	ASleeveBullets* myMagazine = Cast<ASleeveBullets>(GetWorld()->SpawnActor(ASleeveBullets::StaticClass(), &SpawnLocation, &SpawnRotation, SpawnParams));
	if (myMagazine)
	{
		myMagazine->SetStaticMesh(WeaponSetting.SleeveBullets);
		myMagazine->Movement->InitialSpeed = 200;
		myMagazine->Movement->MaxSpeed = 200;
		myMagazine->SetLifeSpan(5);
	}
}
