// Fill out your copyright notice in the Description page of Project Settings.


#include "SleeveBullets.h"

// Sets default values
ASleeveBullets::ASleeveBullets()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	StaticMesh->SetSimulatePhysics(false);
	StaticMesh->SetEnableGravity(false);

	Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Movement"));
	Movement->UpdatedComponent = RootComponent;
	Movement->InitialSpeed = 10000.0f;
	Movement->MaxSpeed = 10000.0f;
	Movement->bRotationFollowsVelocity = true;
	Movement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void ASleeveBullets::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASleeveBullets::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

