// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "TopDownShooterCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	ATopDownShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;
	class UDecalComponent* CurrentCursor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	class UTPSInventoryComponent* InventoryComponent;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMatereal = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovmentState MovmentState = EMovmentState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovmentInfo;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimPressed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkPressed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintPressed;

	UPROPERTY(BlueprintReadOnly, Category = "FireLogic")
		bool Reloading = false;

	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	//Tick
	UFUNCTION()
	void MovmentTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovmentState();

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	//Weapon	
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName InitWeaponName;

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IDWeapon, FAddicionalWeaponInfo WeaponAddicionalInfo);

	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();

	UFUNCTION(BlueprintCallable)
	void AttacCharEvent(bool bIsFiring);

	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim, UAnimMontage* AnimAim, float ReloadTime);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoNeedTake);
	UFUNCTION()
	  void OnWeaponFire(UAnimMontage* Anim, UAnimMontage* AnimAim, float RateOfFire);
	
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim, float PlayRate);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP();	
	UFUNCTION(BlueprintNativeEvent)
		void OnWeaponFire_BP(UAnimMontage* Anim, float PlayRate);

	UFUNCTION()
		void TrySwitchNextWeapon();
	UFUNCTION()
		void TrySwitchPreviosWeapon();
	UFUNCTION(BlueprintCallable)
		void TrySwitchWeaponToIndex(int32 NewIndex);
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

};

