// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSInventoryComponent.h"

// Sets default values for this component's properties
UTPSInventoryComponent::UTPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
					WeaponSlots[i].AddicionalInfo.Round = Info.MaxRound;
				else
				{
					//WeaponSlots.RemoveAt(i);
					//i--;
				}
			}
		}
	}
	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AddicionalInfo);
	}
}


// Called every frame
void UTPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAddicionalWeaponInfo OldInfo)
{
	if (ChangeToIndex == OldIndex)
		return false;
	if (ChangeToIndex >= 0 && ChangeToIndex < WeaponSlots.Num())
	{
		FName NewIdWeapon;
		FAddicionalWeaponInfo NewAdditionalInfo;

		if (!WeaponSlots[ChangeToIndex].NameItem.IsNone())
		{
			NewIdWeapon = WeaponSlots[ChangeToIndex].NameItem;
			NewAdditionalInfo = WeaponSlots[ChangeToIndex].AddicionalInfo;
			SetAddicionalInfoWeapon(OldIndex, OldInfo);
			OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);
			return true;
		}
	}

	return false;
}

bool UTPSInventoryComponent::SwitchWeaponToNext(bool IsPrevious, int32 OldIndex, FAddicionalWeaponInfo OldInfo)
{
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		int8 ChangeToIndex;
		if (IsPrevious)
		{
			ChangeToIndex = OldIndex - 1;
			if (ChangeToIndex < 0)
				ChangeToIndex = ChangeToIndex + WeaponSlots.Num();
		}
		else
		{
			ChangeToIndex = OldIndex + 1;
			if (ChangeToIndex >= WeaponSlots.Num())
				ChangeToIndex = ChangeToIndex - WeaponSlots.Num();
		}


		FName NewIdWeapon;
		FAddicionalWeaponInfo NewAdditionalInfo;

		while (ChangeToIndex != OldIndex)
		{
			if (!WeaponSlots[ChangeToIndex].NameItem.IsNone())
			{
				if (WeaponSlots[ChangeToIndex].AddicionalInfo.Round > 0)
				{
					NewIdWeapon = WeaponSlots[ChangeToIndex].NameItem;
					NewAdditionalInfo = WeaponSlots[ChangeToIndex].AddicionalInfo;
					SetAddicionalInfoWeapon(OldIndex, OldInfo);
					OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);
					return true;
				}
				else
				{
					FWeaponInfo Info;
					if (myGI->GetWeaponInfoByName(WeaponSlots[ChangeToIndex].NameItem, Info))
					{
						for (int i = 0; i < AmmoSlots.Num(); i++)
						{
							if (AmmoSlots[i].WeaponType == Info.WeaponType && AmmoSlots[i].Count > 0)
							{
								NewIdWeapon = WeaponSlots[ChangeToIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[ChangeToIndex].AddicionalInfo;
								SetAddicionalInfoWeapon(OldIndex, OldInfo);
								OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);
								return true;
							}
						}
					}
				}
			}
			if (IsPrevious)
			{
				ChangeToIndex = ChangeToIndex - 1;
				if (ChangeToIndex < 0)
					ChangeToIndex = ChangeToIndex + WeaponSlots.Num();
			}
			else
			{
				ChangeToIndex = ChangeToIndex + 1;
				if (ChangeToIndex >= WeaponSlots.Num())
					ChangeToIndex = ChangeToIndex - WeaponSlots.Num();
			}

		}
	}

	return false;
}

FAddicionalWeaponInfo UTPSInventoryComponent::GetAddicionalInfoWeapon(int32 IndexWeapon)
{
	FAddicionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				result = WeaponSlots[i].AddicionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAddicionalInfoWeapon - �� ����� ������ �� ������� %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAddicionalInfoWeapon - �� ���������� ������ %d"), IndexWeapon);
	return result;
}

int32 UTPSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

FName UTPSInventoryComponent::GetWeaponNameSlotByIndex(int32 indexSlot)
{
	if(WeaponSlots.IsValidIndex(indexSlot))
		return WeaponSlots[indexSlot].NameItem;
	return FName();
}

void UTPSInventoryComponent::SetAddicionalInfoWeapon(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AddicionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAddicionalInfoWeapon - �� ����� ������ �� ������� %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAddicionalInfoWeapon - �� ���������� ������ %d"), IndexWeapon);
}

void UTPSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 AmmoTaken)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i<AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Count += AmmoTaken;
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
			OnAmmoChange.Broadcast(TypeWeapon, AmmoSlots[i].Count);
			bIsFind = true;
		}
		i++;
	}
}

bool UTPSInventoryComponent::CheckAmmoOnWeapon(EWeaponType TypeWeapon, int8& AviableAmmoForReload)
{
	AviableAmmoForReload = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoForReload = AmmoSlots[i].Count;
			if (AmmoSlots[i].Count > 0)
				return true;
		}
		i++;
	}
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);

	return false;
}


bool UTPSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == AmmoType)
		{
			bIsFind = true;
			if(AmmoSlots[i].Count < AmmoSlots[i].MaxCount)
				return  true;
		}
		i++;
	}
	return false;
}


bool UTPSInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot, FName WeaponName)
{
	FreeSlot = -1;
	bool bIsHaved = false;
	int8 i = 0;
	while(i < WeaponSlots.Num())
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			if(FreeSlot == -1)
				FreeSlot = i;
		}
		else
		{
			if (WeaponSlots[i].NameItem == WeaponName)
				return false;
		}
		i++;
	}
	return 	true;
}


bool UTPSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, FDropItem& DropItemInfo)
{
	int32 kek;
	if (CheckCanTakeWeapon(kek, NewWeapon.NameItem) && WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;
		SetAddicionalInfoWeapon(IndexSlot, WeaponSlots[IndexSlot].AddicionalInfo);
		OnSwitchWeapon.Broadcast(WeaponSlots[IndexSlot].NameItem, WeaponSlots[IndexSlot].AddicionalInfo);
		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		return true;
	}
	return false;
}

bool UTPSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 indexSlot = -1;
	if (CheckCanTakeWeapon(indexSlot, NewWeapon.NameItem))
	{
		if (WeaponSlots.IsValidIndex(indexSlot))
		{
			WeaponSlots[indexSlot] = NewWeapon;
			OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
			return true;
		}
	}
	return false;
}

bool UTPSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool Result = false;

	FName DropItemName = GetWeaponNameSlotByIndex(IndexSlot);

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		Result = myGI->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
			DropItemInfo.WeaponInfo.AddicionalInfo = WeaponSlots[IndexSlot].AddicionalInfo;
	}

	return Result;
}
